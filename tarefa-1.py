import time
from selenium import webdriver
from webdriver_manager.chrome import ChromeDriverManager
from selenium.webdriver.chrome.service import Service

servico = Service(ChromeDriverManager().install())

navegador = webdriver.Chrome(service=servico)
navegador.get('https://www.amazon.com.br/')

time.sleep(13)
#PESQUISA MOUSE
navegador.find_element('xpath', '//*[@id="twotabsearchtextbox"]').send_keys('Mouse')
time.sleep(0)

# CLICA PROCURAR
navegador.find_element('xpath', '//*[@id="nav-search-submit-button"]').click()
time.sleep(3)

# CLICA PRODUTO
navegador.find_element('xpath', '//*[@id="search"]/div[1]/div[1]/div/span[1]/div[1]/div[3]/div/div/div/div/span/div/div/div[1]/span/a/div/img').click()
time.sleep(10)

# ADICIONA NO CARRINHO
navegador.find_element('xpath', '//*[@id="add-to-cart-button"]').click()
time.sleep(10)

# CLICA EM IGNORAR
navegador.find_element('xpath', '//*[@id="attachSiNoCoverage"]/span/input').click()
time.sleep(3)

# PESQUISA
navegador.find_element('xpath', '//*[@id="twotabsearchtextbox"]').send_keys('Teclado')
time.sleep(10)

# CLICA PROCURAR
navegador.find_element('xpath', '//*[@id="nav-search-submit-button"]').click()
time.sleep(3)


# CLICA PRODUTO
navegador.find_element('xpath', '//*[@id="search"]/div[1]/div[1]/div/span[1]/div[1]/div[3]/div/div/div/div/span/div/div/div[2]/div[1]/h2/a/span').click()
time.sleep(10)

# ADICIONA NO CARRINHO
navegador.find_element('xpath', '//*[@id="ape_Detail_desktop-detail-ilm_desktop_placement_Feedback"]').click()
time.sleep(3)


# CLICA EM IGNORAR
navegador.find_element('xpath', '//*[@id="attachSiNoCoverage"]/span/input').click()
time.sleep(15)


# CLICA NO CARRINHO
navegador.find_element('xpath', '//*[@id="nav-cart-text-container"]/span[2]').click()
time.sleep(7)


